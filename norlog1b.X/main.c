#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/eusart.h"
#include "nortec/nortec_rx.h"

#define MAXLEN 16
char line[MAXLEN]; // output buffer
byte lofs = 0; // line offset

byte posOurLast = LOG_SIZE - 1;


// Serial printing

#define printch(x) EUSART_Write((byte)(x))

void print(char *text) {
    while (*text) {
        printch(*text);
        text++;
    }
}


// Hex print raw data

void printHex(byte *rcvPtr, byte len) {
    byte c, n, k;
    for (k = 0; k < len; k++) {
        c = rcvPtr[k];
        n = c >> 4;
        printch(n > 9 ? 55 + n : 48 + n);
        n = c & 0x0f;
        printch(n > 9 ? 55 + n : 48 + n);
    }
}

void printHexWord(word w) {
    byte n;
    n = (w >> 12) & 0x0f;
    printch(n > 9 ? 55 + n : 48 + n);
    n = (w >> 8) & 0x0f;
    printch(n > 9 ? 55 + n : 48 + n);
    n = (w >> 4) & 0x0f;
    printch(n > 9 ? 55 + n : 48 + n);
    n = w & 0x0f;
    printch(n > 9 ? 55 + n : 48 + n);
}


// Arduino-like setup

void setup() {
    TMR0_SetInterruptHandler(handle_rx433); // timer 100us
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    print("\r\nnor-tec receiver\r\n");
    IO_RA4_SetHigh(); // turn on receiver
    TMR0_StartTimer();
}


// Arduino-like main loop

long loopCounter = 0;
word battery_voltage = 0;

void loop() {
    if (++loopCounter > 200000) {
        bool tmp = received;
        received = true; // block IRQ
        word bv = ADC_GetConversion(channel_FVR);
        if (bv != battery_voltage){
            printch('\r');
            printch('\n');
            printch('v');
            printch(',');
            printHexWord(bv);
            printch(',');
            battery_voltage = bv;
        } else {
            printch('.');
        }
        loopCounter = 0;
        received = tmp;
    }

    if (!received) {
        // TODO send CPU to sleep
        return;
    }

    printch('\r');
    printch('\n');
    printch('a' + posFirstReceived);
    while (posOurLast != posFirstReceived) {
        if (++posOurLast >= LOG_SIZE) posOurLast = 0;
        byte *rp = ring_buffer + (posOurLast * LEN_DATAGRAM_BYTES);
        printch(',');
        printch('a' + posOurLast);
        printch(',');
        printch(nortec_crc_ok(rp) ? 'k' : '?');
        printch(',');
        printHex(rp, LEN_DATAGRAM_BYTES);
    } // while
    printch(',');

    received = false; // reset flag
}


// Application main

void main(void) {
    SYSTEM_Initialize();
    setup();
    while (1) {
        loop();
    }
}

// eof