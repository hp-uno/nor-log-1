#ifndef NORTEC_RX_H
#define	NORTEC_RX_H
#ifdef	__cplusplus
extern "C" {
#endif

//// NORTEC DECODER

#define LOG_SIZE           16
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5

// map Arduino data types
typedef uint8_t byte;
typedef uint16_t word;
typedef word dur_t;

extern volatile bool received;
extern byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
extern byte posFirstReceived;
extern byte posCurrReceiving;

// RX433 Interrupt Handlers
extern void handle_rx433(void);
// CRC check function
extern bool nortec_crc_ok(byte *rcvPtr);


#ifdef	__cplusplus
}
#endif

#endif	/* NORTEC_RX_H */

