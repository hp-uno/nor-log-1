//// NORTEC CRC
#include <xc.h>
#include "../mcc_generated_files/mcc.h"
#include "nortec_rx.h"

static void memcpy(byte *d, byte *s, byte len) {
    while (len) {
        *d++ = *s++;
        len--;
    }
}

bool nortec_crc_ok(byte *rcvPtr) {
    byte m[LEN_DATAGRAM_BYTES];
    memcpy(m, rcvPtr, LEN_DATAGRAM_BYTES);
    byte msg_crc = m[1] >> 4;
    m[1] = (m[4] << 4) | (m[1] & 0x0F); // for computation, channel info (last nibble) is at CRC position
    byte new_crc = m[0] >> 4; // CRC preload = 0, therefore we can skip these 4 bits
    for (int k = 4; k < 36; k++) {
        new_crc <<= 1;
        if (m[k >> 3] & (0x80 >> (k & 7))) {
            new_crc |= 1;
        }
        if (new_crc & 0x10) {
            new_crc ^= 0x13;
        }
    }
    return (new_crc == msg_crc);
}

