// Nor-tec log receiver (PIC version))
// 2021-05-11 Hagen Patzke

// PIC16F18313 Configuration Bit Settings

// CONFIG1
#pragma config FEXTOSC = OFF    // FEXTOSC External Oscillator mode Selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINT1  // Power-up default value for COSC bits->HFINTOSC (1MHz)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; I/O or oscillator function on OSC2)
#pragma config CSWEN = ON       // Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR/VPP pin function is MCLR; Weak pull-up enabled )
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config WDTE = OFF       // Watchdog Timer Enable bits (WDT disabled; SWDTEN is ignored)
#pragma config LPBOREN = OFF    // Low-power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable bits (Brown-out Reset enabled, SBOREN bit ignored)
#pragma config BORV = HIGH      // Brown-out Reset Voltage selection bit (Brown-out voltage (Vbor) set to 2.7V)
#pragma config PPS1WAY = OFF    // PPSLOCK bit One-Way Set Enable bit (The PPSLOCK bit can be set and cleared repeatedly (subject to the unlock sequence))
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a Reset)
#pragma config DEBUG = OFF      // Debugger enable bit (Background debugger disabled)

// CONFIG3
#pragma config WRT = OFF        // User NVM self-write protection bits (Write protection off)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low voltage programming enabled. MCLR/VPP pin function is MCLR. MCLRE configuration bit is ignored.)

// CONFIG4
#pragma config CP = OFF         // User NVM Program Memory Code Protection bit (User NVM code protection disabled)
#pragma config CPD = OFF        // Data NVM Memory Code Protection bit (Data NVM code protection disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

// Tell the compiler the working frequency (needed for _delay functions)
#define _XTAL_FREQ 4000000

#define OUT_SIG 0b00100000 // RA5, pin 2
#define OUT_GAP 0b00000000 // all low

void setup(void) {
    LATA    = OUT_GAP;    // all output latches low
    TRISA   = 0b00000000; // RA0/1/2/4/5 output (RA3=MCLR)
    ANSELA  = 0b00000000; // no analog inputs
    WPUA    = 0b00000000; // No weak pull-ups (no input)
    ODCONA  = 0b00000000; // Open Drain Control: all outputs are push-pull
    SLRCONA = 0b00000000; // Slew-rate as fast as possible
    INLVLA  = 0b00000000; // All inputs TTL level
    PMD0    = 0x00; // CLKRMD CLKR enabled; SYSCMD SYSCLK enabled; FVRMD FVR enabled; IOCMD IOC enabled; NVMMD NVM enabled; 
    PMD1    = 0x00; // TMR0MD TMR0 enabled; TMR1MD TMR1 enabled; TMR2MD TMR2 enabled; NCOMD DDS(NCO) enabled; 
    PMD2    = 0x00; // DACMD DAC enabled; CMP1MD CMP1 enabled; ADCMD ADC enabled; 
    PMD3    = 0x00; // CCP2MD CCP2 enabled; CCP1MD CCP1 enabled; PWM6MD PWM6 enabled; PWM5MD PWM5 enabled; CWG1MD CWG1 enabled; 
    PMD4    = 0x00; // MSSP1MD MSSP1 enabled; UART1MD EUSART enabled; 
    PMD5    = 0x00; // DSMMD DSM enabled; CLC1MD CLC1 enabled; CLC2MD CLC2 enabled; 
    OSCCON1 = 0x60; // NOSC HFINTOSC, NDIV=1
    OSCCON3 = 0x00; // CSWHOLD may proceed; SOSCPWR Low power; SOSCBE crystal oscillator; 
    OSCEN   = 0x00; // LFOEN disabled; ADOEN disabled; SOSCEN disabled; EXTOEN disabled; HFOEN disabled; 
    OSCFRQ  = 0x03; // HFFRQ 4_MHz; 
    OSCTUNE = 0x00; // HFTUN 0; 
    WDTCON  = 0x16; // WDTPS 1:65536; SWDTEN OFF;
    
}


int getVdd(){
    // Initialize Forward Voltage Reference to 1.024V
    FVRCON = 0b10000001; // Buffer gain is 1x, enable FVR
    // Initialize and enable ADC
    ADCON0 = 0b11111101; // b0:ADC ON, b1:ADC go/-done off, b2..7:FVR
    ADCON1 = 0b11000000; // b7:right-justified, b6..4:Fosc/4, b2:ADNref=Vss, b10:ADPREF=Vdd
    
    // need to wait until FVRCON bit 6 becomes 1
    while(!(FVRCON & 1<<6))/*wait*/;
    // ca. 35us should be long enough to meet acquisition time requirements
    
    ADCON0 |= 2; // ADC go: start conversion
    while(ADCON0 & 2)/*wait*/;
    
    // get 10-bit conversion result
    int result = ADRESH << 8 | ADRESL;
    
    // turn off ADC and FVR
    ADCON0 = 0b11111100;
    FVRCON = 0b00000001;
    
    return result; // should be between 3.25V(322) and 4.2V(250). 5V (210) is too high.
}


void setupTimers(){
    // For a microsecond timer, we need to divide the 4MHz clock by four and count.
    // One 16bit timer can cover 64 milliseconds.
}


void main() {
    
    
}