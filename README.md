# PIC-based nor-tec logger

Looking at oscilloscope pictures from 432.920 MHz receivers (RX433 modules[1]), I could see quite some erratic waveforms.

[1] (433MHz here is used as a shorthand for 432,92MHz which is in the middle of the Industrial, Scientific and Manufacturing (ISM) frequency band)

On closer inspection, these seemed to be caused (or at least made much worse) by ripple on the power supply rail.

For my tests, I used a USB power bank. Inside these power banks is usually a 18650 Lithium-Ion battery cell plus a charger/boost converter circuit.

The circuit

- converts 5V from USB for charging to ca. 4.2V for the Lithium-Ion cell
- converts the 3.7V..4.2V from the Lithium-Ion cell to 5V on the USB output socket

All of these circuits use a 'boost converter' to transform a lower voltage to a higher voltage.
To do this, the direct current from the cell needs to be 'switched on' and then fed to a coil, which generates a magnetic field.
When the power to the coil is 'switched off', the magnetic field wants to continue the current flow.
As it breaks down, it induces a voltage spike that can be much higher than the input voltage fed initially to the coil.

> This is the reason why digital circuits need protection diodes when they are connected to any electro-magnetic device - with out them, the high voltage generated when the digital output switches 'off' power would destroy the semiconductor circuit.

Turning something periodically on and off requires an oscillator, so these power banks generate their own form of internal Electro-Magnetic Interference (EMI).
And even with reasonably-sized electrolytic capacitors to suppress this 'ripple' on the power rail, a bit of it will remain.

This 'ripple' can influence RF signal detection. Here is how.

Looking at several 433MHz receiver circuits, they have one thing in common: they have an Automatic Gain Control circuit.

AGC has the purpose of 'cranking up' the sensitivity of a circuit if there is a weak input signal, and 'turning it down' when there is a strong input signal.

AGC is a good and important function (every FM radio has one), but it also means that if there is currently no input signal, the AGC will turn up receiver sensitvity so much that all you get is 'noise'.

If sensitivity is up a lot and there is a noise source nearby, that noise will likely show up on the digital output of the RX433 receiver module.Switching power supply ripple is 'noise', so it should show up. And it does.

How can we prevent this?

One way is to power the receiver circuit from the 3.7V Lithium-Ion cell directly, not with the output from a boost converter.

But the Arduino I use to display the received nor-tec data on an OLED and to store it on an SD card would not run from the 3.7V battery directly.

And that is also not necessary, if we use a different microcontroller, e.g. a Microchip PIC, for the RX433 stuff:

- a PIC chip only needs about 1-2 mA for its operation, and the ones I have work from 2.3V to 5.5V
- the 3V-compatible RX433 MHz receiver module I have works from 2.7V to 5.3V (my other RX433 modules need 5V)

This has additional benefits:

- the RX433 receiver module can be detached from the Arduino, i.e. far away from the SD1366 OLED module (which also has a boost converter built-in)
- the PIC can do all of the reception/decoding/datagram checking work and only 'wake up' the Arduino when there is fresh data (or at a fixed interval)
- the PIC can monitor the Lithium-Ion cell voltage and turn off the RX433 receiver module and itself if the voltage is too low

---

ADCcount for different Vdd:

| `maxCount` |  Vdd | ADCcount |
| ---------- | ---: | -------: |
| 4096       | 5.00 |   838.86 |
| `Vin`      | 4.20 |   998.64 |
| 1.024      | 3.70 |  1133.60 |
| `maxVal`   | 3.50 |  1198.37 |
| 4194.304   | 3.25 |  1290.56 |

PIC with Vref=Vdd and Vin=1.024V bandgap

Vdd = (1.024 \* 4096)/ADCval (12-bit)

Vdd = (4194.304)/ADCval =ca. 4194/ADCval

https://electronics.stackexchange.com/questions/160570/measure-pics-own-vdd-in-voltage-with-adc

That PIC has an internal 1.024V bandgap reference, selected by setting the Channel Select bits to 11111.
So if you set your ADC module's reference to Vdd and then tell it to measure the 1.024V reverence, you can infer back to what your Vdd actually is.

Normally you would work out the voltage on a pin which you've A2D'd with something like:
Vin = (ADCval/ADCrange)\*Vref

But in this case its Vin which you know (1.024v) and you want to solve for Vref (your Vdd), so:
Vref = (Vin _ ADCrange)/ADCval
or more specifically for your case:
Vdd = (1.024 _ 4096)/ADCval
