#ifndef NORTEC_RX_H
#define	NORTEC_RX_H
#ifdef	__cplusplus
extern "C" {
#endif

    //// NORTEC DECODER


    // map Arduino data types
    typedef uint8_t byte;
    typedef uint16_t word;
    typedef byte dur_t;

    // Nortec datagram is 5 bytes long
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5

#define DATAPIN     5 // RA5
#define LOG_SIZE    16

    // pulse-pause modulation:
    // after a fixed-length pulse comes a short or a long pause before the next pulse
    // 0.5ms pulse width
    // 2ms pause = 0, 4ms pause = 1, 8ms pause = start, 16ms pause = end
    // observed parameters:
    // pre=500uS, bit0=1968uS, bit1=3980uS, start_gap=7976uS, end_gap=15976uS
#define LEVEL_PRE    1     // level of the fixed width prefix pulse
    // sample rate of 100us
#define LEN_PRE_MIN  4
#define LEN_PRE_MAX  6
#define LEN_BI0_MIN  12
#define LEN_BI1_MIN  32
#define LEN_STA_MIN  70
#define LEN_END_MIN  100


    extern byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
    extern byte posCurrReceiving;
    extern volatile bool received;

    // RX433 Interrupt Handlers
    extern void handle_tim0(void);

#ifdef	__cplusplus
}
#endif

#endif	/* NORTEC_RX_H */

