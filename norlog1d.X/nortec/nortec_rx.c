//// NORTEC DECODER

#include <xc.h>
#include "../mcc_generated_files/mcc.h"
#include "nortec_rx.h"

volatile bool received;
byte ring_buffer[LOG_SIZE * LEN_DATAGRAM_BYTES];
byte posCurrReceiving = 0;
byte posFirstReceived = LOG_SIZE - 1;

#define sleep_disable() /*no-op*/

// Sample input every 100us with the timer0 interrupt.
// Count successive bits (0 or 1) and process them, once polarity changes.

void handle_tim0(void) {
    static bool curLevel = false;
    static dur_t curPeriod = 0;
    static dur_t durationNow = 0;
    static byte posCurBit = 0;
    static bool gotPulse = false;

    if (received) // DEBUG ONLY, normally we want to receive in parallel to processing
        return;

    if (curLevel == !IO_RA5_GetValue()) {
        curPeriod++;
        return;
    }

    curLevel = !curLevel;
    durationNow = curPeriod;
    curPeriod = 0;

    if (durationNow < LEN_PRE_MIN || durationNow > LEN_END_MIN) { // noise, end-of-datagram gap, timeout
        posCurBit = 0; // reset bit position within datagram
        gotPulse = false;
        return;
    }

    if (gotPulse) {
        gotPulse = false;
        if (durationNow > LEN_STA_MIN) { // start-of-datagram gap
            posCurBit = 0;
            //timestamp[posCurrReceiving] = timeNow;
            return;
        } else if (durationNow < LEN_BI0_MIN) { // gap length between start pulse and 0-bit
            posCurBit = 0;
        } else { // we have a data bit
            byte *p = &ring_buffer[(posCurrReceiving * LEN_DATAGRAM_BYTES) + (posCurBit >> 3)];
            byte m = 0x80 >> (posCurBit & 7); // bit mask
            if (durationNow >= LEN_BI1_MIN) { // it is a one
                *p |= m;
                posCurBit++;
            } else { // it is a zero
                *p &= ~m;
                posCurBit++;
            }
            // check if the datagram is now complete
            if (posCurBit >= LEN_DATAGRAM_BITS) { // we have all bits
                // timing does not matter anymore at this point
                posCurBit = 0;
                if (!received) { // no output running, log start pos of first received datagram
                    posFirstReceived = posCurrReceiving;
                    received = true;
                    sleep_disable(); //need to disable sleep on Arduino for loop)= to continue
                }
                if (++posCurrReceiving >= LOG_SIZE) posCurrReceiving = 0;
            }
        }
    }

    // check if we got a start pulse
    if ((durationNow >= LEN_PRE_MIN) && (durationNow <= LEN_PRE_MAX)) {
        gotPulse = true;
    }
}
