EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR?
U 1 1 608333A5
P 7450 1050
F 0 "#PWR?" H 7450 900 50  0001 C CNN
F 1 "+5V" H 7465 1223 50  0000 C CNN
F 2 "" H 7450 1050 50  0001 C CNN
F 3 "" H 7450 1050 50  0001 C CNN
	1    7450 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60833B79
P 7450 3250
F 0 "#PWR?" H 7450 3000 50  0001 C CNN
F 1 "GND" H 7455 3077 50  0000 C CNN
F 2 "" H 7450 3250 50  0001 C CNN
F 3 "" H 7450 3250 50  0001 C CNN
	1    7450 3250
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_PIC16:PIC16F18313-IP U1
U 1 1 60837076
P 7450 2050
F 0 "U1" H 7450 2831 50  0000 C CNN
F 1 "PIC16F18313-IP" H 7450 2740 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8050 2700 50  0001 C CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/40001799F.pdf" H 7450 2050 50  0001 C CNN
	1    7450 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1450 7450 1200
Connection ~ 7450 1200
Wire Wire Line
	7450 1200 7450 1050
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 609A7772
P 8950 2050
F 0 "J1" H 8978 2026 50  0000 L CNN
F 1 "Conn_RX433" H 8978 1935 50  0000 L CNN
F 2 "" H 8950 2050 50  0001 C CNN
F 3 "~" H 8950 2050 50  0001 C CNN
	1    8950 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2650 7450 2800
Wire Wire Line
	8550 2350 8550 2800
Wire Wire Line
	8550 2800 7450 2800
Connection ~ 7450 2800
Wire Wire Line
	7450 2800 7450 3250
Wire Wire Line
	8750 2350 8550 2350
Wire Wire Line
	8750 2150 8050 2150
$Comp
L Device:Antenna AE?
U 1 1 609AC7CD
P 8550 1000
F 0 "AE?" H 8630 989 50  0000 L CNN
F 1 "Antenna" H 8630 898 50  0000 L CNN
F 2 "" H 8550 1000 50  0001 C CNN
F 3 "~" H 8550 1000 50  0001 C CNN
	1    8550 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1850 8550 1850
Wire Wire Line
	8550 1850 8550 1200
Wire Wire Line
	7450 1200 5850 1200
Wire Wire Line
	5850 1200 5850 1900
Wire Wire Line
	5850 2200 5850 2800
Wire Wire Line
	5850 2800 7450 2800
Wire Wire Line
	8050 2050 8750 2050
Text Notes 8600 2050 0    50   ~ 0
Vcc
Text Notes 8600 1850 0    50   ~ 0
ANT
Text Notes 8600 2150 0    50   ~ 0
DATA
Text Notes 8600 2350 0    50   ~ 0
GND
$Comp
L Device:Battery_Cell BT
U 1 1 609AD411
P 5850 2100
F 0 "BT" H 5968 2196 50  0000 L CNN
F 1 "LiIon_3.7V" H 5968 2105 50  0000 L CNN
F 2 "" V 5850 2160 50  0001 C CNN
F 3 "~" V 5850 2160 50  0001 C CNN
	1    5850 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 609B8EC7
P 8150 1450
F 0 "R1" H 8220 1496 50  0000 L CNN
F 1 "100K" H 8220 1405 50  0000 L CNN
F 2 "" V 8080 1450 50  0001 C CNN
F 3 "~" H 8150 1450 50  0001 C CNN
	1    8150 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1300 8150 1200
Wire Wire Line
	8150 1200 7450 1200
Wire Wire Line
	8150 1600 8150 1950
Wire Wire Line
	8150 1950 8050 1950
$EndSCHEMATC
