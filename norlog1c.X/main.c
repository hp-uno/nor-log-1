/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.6
        Device            :  PIC16F18313
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/eusart.h"
#include "nortec/nortec_rx.h"

byte posOurLast = 1;

#define MAXLEN 16
char line[MAXLEN]; // output buffer
byte lofs = 0; // line offset


#define printch(x) EUSART_Write((byte)(x))

void print(char *text) {
    while (*text) {
        EUSART_Write((byte) * text);
        text++;
    }
}

// Hex print raw data

void printHex(word c) {
    byte n, k;
    n = (c >> 12) & 0x0F;
    printch(n > 9 ? 55 + n : 48 + n);
    n = (c >> 8) & 0x0F;
    printch(n > 9 ? 55 + n : 48 + n);
    n = (c >> 4) & 0x0F;
    printch(n > 9 ? 55 + n : 48 + n);
    n = c & 0x0F;
    printch(n > 9 ? 55 + n : 48 + n);
}

void setup() {
    // set nor-tec receive handler
    IOCAF5_SetInterruptHandler(handle_rx433); // pin level change (RA5))
    TMR0_SetInterruptHandler(handle_tim0); // timeout (65ms)
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    print("\r\nnor-tec receiver\r\n");
    IO_RA4_SetHigh(); // turn on receiver
    TMR0_StartTimer();
}

void loop() {
    if (!received) return;

    for (byte i = 0; i < LOG_SIZE; i++) {
        printHex(recdur[i]);
        printch(' ');
    }
    printch('\n');
    printch('\r');

    received = false;
}


// Application

void main(void) {
    SYSTEM_Initialize();
    setup();
    while (1) {
        loop();
    }
}
// eof