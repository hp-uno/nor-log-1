//// NORTEC DECODER

#include <xc.h>
#include "../mcc_generated_files/mcc.h"
#include "nortec_rx.h"

word recdur[LOG_SIZE];
byte posCurrReceiving = 0;
volatile bool received = false;


void process_duration(word duration){
    if (received) 
        return;

    recdur[posCurrReceiving] = duration;
    posCurrReceiving++;
    if (posCurrReceiving >= LOG_SIZE) {
        received = true;
        posCurrReceiving = 0;
    }
}


// RX433 Module Signal Timeout Handler

void handle_tim0(void) {
    process_duration(0xFFFF);
}

// RX433 Module Signal Change Interrupt Handler

void handle_rx433(void) {
    byte levelNow = IO_RA5_GetValue();
    word durationNow = TMR0_ReadTimer();
    TMR0_Reload();
    process_duration((durationNow & 0xFFFE) | (levelNow & 1));
} // end handle_rx433
