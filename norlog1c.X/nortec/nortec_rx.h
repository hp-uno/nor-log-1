#ifndef NORTEC_RX_H
#define	NORTEC_RX_H
#ifdef	__cplusplus
extern "C" {
#endif

    //// NORTEC DECODER


    // map Arduino data types
    typedef uint8_t byte;
    typedef uint16_t word;
    typedef word dur_t;


#define DATAPIN     5 // RA5
    // Nortec transmitters use codes 1..3
#define LOG_SIZE    80

    // Nortec datagram is 5 bytes long
#define LEN_DATAGRAM_BITS  40
#define LEN_DATAGRAM_BYTES 5
    // pulse-pause modulation:
    // after a fixed-length pulse comes a short or a long pause before the next pulse
    // 0.5ms pulse width
    // 2ms pause = 0, 4ms pause = 1, 8ms pause = start, 16ms pause = end
    // observed parameters:
    // pre=500uS, bit0=1968uS, bit1=3980uS, start_gap=7976uS, end_gap=15976uS
#define LEVEL_PRE    1     // level of the fixed width prefix pulse
#define LEN_PRE_MIN  400
#define LEN_PRE_MAX  600
#define LEN_BI0_MIN  1250
#define LEN_BI1_MIN  3250
#define LEN_STA_MIN  7000
#define LEN_END_MIN  10000


    extern word recdur[LOG_SIZE];
    extern byte posCurrReceiving;
    extern volatile bool received;

    // RX433 Interrupt Handlers
    extern void handle_rx433(void);
    extern void handle_tim0(void);

#ifdef	__cplusplus
}
#endif

#endif	/* NORTEC_RX_H */

